<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');
Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidates.changestatus')->middleware('auth');
Route::post('candidates/changestatus/', 'CandidatesController@changeStatusFromCadidate')->name('candidates.changestatusfromcandidate')->middleware('auth');

Route::get('users', 'UsersController@index')->name('users.index')->middleware('auth');
Route::get('users/{id}/edit', 'UsersController@edit')->name('users.edit')->middleware('auth');
Route::post('users/{id}/update/', 'UsersController@update')->name('users.update')->middleware('auth');
Route::get('users/{id}/delete/', 'UsersController@destroy')->name('user.delete')->middleware('auth');


Route::get('mycandidates', 'CandidatesController@myCandidates')->name('candidates.mycandidates')->middleware('auth');
Route::get('users/add', '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('users.add')->middleware('auth');
Route::post('users/create', '\App\Http\Controllers\Auth\RegisterController@adduser')->name('users.create')->middleware('auth');


Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete')->middleware('auth');
Route::get('candidates/sorted/{by}', 'CandidatesController@indexsorted')->name('candidate.sorted')->middleware('auth');

Route::get('/hello', function (){
    return 'Hello Larevel';
}); 
































Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
